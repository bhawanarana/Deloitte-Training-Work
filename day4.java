/*hardware dependent on os
as hardware is changing we need more new os
bytecode is the object code
Features of java
Object oriented
robust
secure
Multithreaded
--------------------------------------------------------------------

Rules to write java program


it is case sensitive
java file should have .java extension
First statement should be package declaration if required
instruction must be inside a method and a method inside a class
every class can have main method but the class that had got public will be executed else it will give error
to create different classes public we need to make different files
a public class have multiple main method but only the one with correct signature works
java is a block structure
eol is semicolon(;)
filename should be the same as public class name
------------------------------------------------------------------------

Setting java Environment


copy jdk bin path
go to this pc properties
advanced system settings
click environment variables
save path in new user variable

---------------------------------------------------------------------------

Naming Convention


Class Name= IndianBank, BankOfIndia
method name=indianBank,bankOfIndia
variable or property= indian_bank ,bank_of_india
constants= INDIANBANK
Packages=indian.bank,bank.of.india


-------------------------------------------------------------------------------
Program */

class Demo1 {
	public static void main(String args[]){
		System.out.println("Welcome to Java");
	}
}


//javap class file name will display all methods used in the specified class file


//Accept two numbers from the command prompt and display sum of two numbers



class CommandSum {
	public static void main(String args[]) {

		if(args.length==2){

		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);

		int c = a + b;

		System.out.print("Sum of "+a+" and "+b+" is "+c);
	}
	else
	{
		System.out.print("Invaid No. of args");
	}
}
}

/*open eclipse
set perspective
create project
select default jre
go to configure JREs
*/


-------------------------------------------------------------------------------------------



/*accept two float values and sum them*/


package p1;

public class FloatSum {
	public static void main(String args[]) {

		if(args.length==2){

		float a = Float.parseFloat(args[0]);
		float b = Float.parseFloat(args[1]);

		float c = a + b;

		System.out.print("Sum of "+a+" and "+b+" is "+c);
	}
	else
	{
		System.out.print("Invaid No. of args");
	}
}

}



/*
accept two integers and operator and do operations based on the operator*/




package p1;

public class Operations {
	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		char c= args[2].charAt(0);
		if (args.length==3) {
	
		switch(c)
		{
		case '+':
			System.out.println("Result of operation is "+(a+b));
			break;
		case '-':
		System.out.println("Result of operation is "+(a-b));
			break;
			
		case '/':
			if(b==0) {
				System.out.println("Division by 0 not possible");
			}
			else {
			System.out.println("Result of operation is "+(a/b));
			}
			break;
			
		case 'x':
		System.out.println("Result of operation is "+(a*b));
			break;
			
		case '%':
			System.out.println("Result of operation is "+(a%b));
			break;
			
			default:
				System.out.println("Invalid operator");
			
			
			
		}
		}
		else {
			System.out.println("Invalid no. of args");
		}
		
		
	}

}


/* Read n names from cmd  line args print all the names using for and for each program
in the for each loop you will display the name that starts with letter a.Print the biggest name.Name can contain speciaL CHARACTERS */


public class JavaLearn {
public static void main(String[] args) {
	
	String wordLength="";
	for(int i=0;i<args.length;i++) {
		System.out.println(args[i]);
		}
	for (String name: args) {
		if(name.charAt(0) =='a' || name.charAt(0) =='A') {
		System.out.println(name);
		}
	}
		
		for (int i = 0; i < args.length; i++) {
			if(wordLength.length()<args[i].length()) {
				wordLength=args[i];
			}
		}
		System.out.println("longest name="+wordLength);
	
}
}



// annotations(@) are like preprocessor directives in C 

//Accept integer and convert the integer into binary, octal and hexadecimal (use suitable API).create a class which to use its object for implenting scanner class


package p1;

import java.util.Scanner;

public class Read {
	static Scanner obj=new Scanner(System.in);

}



public class WrapperScanner {
	public static void main(String[] args) {
		System.out.println("enter an integer");
		int num1=Read.obj.nextInt();
		
		System.out.println("Binary "+ Integer.toBinaryString(num1));
		System.out.println("Octal "+Integer.toOctalString(num1));
		System.out.println("Hexadecimal "+Integer.toHexString(num1));
		
		
	}

}


//create a class which has the following
//which contains a method add() and contains a method check prime() execute this clause using a main method also test the class using j unit testing 



package p1;


import java.util.Scanner;

public class MethodDefinition {
	
	int add(int a,int b) {
		return(a+b);
	}
	
	int checkPrime(int i) {
          int count = 0;
		for (int k=2; k<i;k++) {
			
			if(i%k==0) {
				count++;
				}
			
			}
		return count;
		}
	}





public class MainMethod {
	public static void main(String[] args) {
		Scanner obj=new Scanner(System.in);
		System.out.println("Enter numbers to be added");
		int num1=obj.nextInt();
		int num2=obj.nextInt();
		
		MethodDefinition obj1= new MethodDefinition();
		int c= obj1.add(num1,num2);
		System.out.println(" sum of two numbers is "+c);
		
		System.out.println("Enter a number to check whether it is prime or not ");
		int num3=obj.nextInt();
		int d=obj1.checkPrime(num3);
		
		if(d==0) {
			System.out.println(num3+" is a prime number");
		}
		else {
			System.out.println(num3+" is not a prime number");
		}
	}

}




//create the jar file of npprocessing class create another project import the jar file and run the npprocessing class in another project





/* Write a java  class which contains an array of n numbers . Create a method for 
a) read data into the array
b) display the array elements
c) sort method
d) search method
e)method to show all prime numbers; */


package p1;

import java.util.Arrays;
import java.util.Scanner;

public class Arraydefi {
	

	void reads(int a[]){
		for (int i = 0; i < a.length; i++) {
			a[i]=Read.obj.nextInt();
			
		}
		prints(a);
	}
	void prints(int b[]) {
		for (int i = 0; i < b.length; i++) {
			System.out.println(b[i]);
			}
		}
	void showPrime(int c[]) {
		MethodDefinition ob=new MethodDefinition();
		for (int i = 0; i < c.length; i++) {
			if(ob.checkPrime(c[i])==1) {
				System.out.println(c[i]);
		}
			}
			
		}
	void search(int a[],int key) {
		int x=Arrays.binarySearch(a, key);
		if(x<0) {
			System.out.println(" No. not found");
		}
		else {
			System.out.println(key+" Number found");
		}
		
	}
	
	void sort(int a[]) {
		Arrays.sort(a);
	}
		
	}




public class MethodDefinition {
	
	int add(int a,int b) {
		return(a+b);
	}
	
	int checkPrime(int i) {
          int count = 0;
		for (int k=2; k<i;k++) {
			
			if(i%k==0) {
				count++;
				}
			
			}
		return count;
		}
	}



public class Arrayo {
	
		public static void main(String[] args) {
			
		System.out.println("Enter the number of elements");
		int size=Read.obj.nextInt();
		int arr[]=new int[size];
		 Arraydefi ob=new  Arraydefi();
		ob.reads(arr);
		ob.showPrime(arr);
		ob.search(arr,10);
		ob.sort(arr);
		ob.showPrime(arr);
	
		
	}
	}







