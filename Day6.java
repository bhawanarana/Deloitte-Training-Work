Constructors

Managerial Method that does not have a return type not even void
If the return type is used, it becomes a method .
It has the same name as the class including case sensitivity.
Called automatically at the time of object creation only once in the life cycle.
Constructors can be overloaded
More is the constructor more is the programs efficiency.
Initialises instance variables.
It is mainly used to return object pointers.
An object cannot be created if thre is no constructor in java.
Hence, if the user does not give the constructor compiler creates a defaut constructir of 0 arguments.
If user creates a constructor default constructor is not created by the compiler and user should create the object with existing constructor or default constructor should be explicitly created.
A constructor can call another constructor or another method but a method cannot call another constructor.
A constructor calls another constructor using "this" for current class and "super" for superclass.
limitation: it must be the first statement.
We cannot have super and this in together.
A default constructor has only one keyword that is "super" which is a call to the super class constructor.
 Java supports copy constructor.: A constructor that copies the data of one object to another object of the same class.

 A class with no name is anonymous class 
 Since java supports anonymous class so the contructor for anonymous class is anonymous block. However, we can also have anonymous block and contructor in a named class. In this case anonymous block will execute first then the constructor.
 If there is more than one anonymous block, top to bottom all the anonymous blocks will be executed first.
 Two types of anonymous block  instance anonymous block and class anonymous block.
 Class block will work first then the instance block.
 Class block works only once in the entire application. However instance block will work for every object.


 Create a class for integer array with following facilities
 -if the size is not specified then default size of the array should be with 10 elements
 -if the size is specified then array sholud be of specified size.
 -It should be able to adopt another array
 -It should have a copy constructor
 -create a method for display horizontally.


 Create class to display area 
 if class takes no args object undefined should come 
 	if it takes 3 parameters 
 		area of triangle
 	if 2 parameters,print the area  of rectangle
 		if 1 parameter(float) , print the area of square
 			if 1 parametr (int), print the area of square



 package p3;

public class Areas {
	private float area;
	private String object;
	public Areas() {
	area=0.0f;
	object="Undefined";
	}
	public Areas(int x, int y) {
		area=x*y;
		object="rectangle";
	}
	public Areas (int x) {
		area=x*x;
		object="square";
	}
	public Areas(int x,int y,int z) {
		double s =(x+y+z)/2;
		area=(float)Math.sqrt(s*(s-x)*(s-y)*(s-z));
		object="triangle";
	}

	public void display() {
		System.out.println(object+" "+area);
}
	
	
	public void finalize() {
		System.out.println("in destructor "+this);
	}
	}


public class Demo1 {

	public static void main(String[] args) {
	Addition ob=new Addition();
	System.out.println(ob.add(10, 20));
	System.out.println(ob.add(10, 20,30));
	System.out.println(ob.add(10.2f, 20.5f));
	System.out.println(ob.add(10.2f, 5));
	System.out.println(ob.add(10, 20.4f));

	


	}

}



 				Garbage Collection

Destructor: A method that gets  executed just before the Garbage collection.
 Dereferencing an object is called garbage Collection.
 It is done by assigning null to objects;
 by referencing object to another object.


 Dispose Technique:
 Specifying the destructor to release all the resources before the object goes for Garbage collection.
By using finalize();


Demonthread:
running at the back;



Access Specifier


                          Same class      Same package   Outside Package  
   private                    yes             no           no

   default                    yes             yes          no

   protected                  yes             yes          no,yes(if it is only inherited)

   public                     yes             yes          yes


   Class is public and constructor is default 
   Class is public and constructor is private


   protected members cannot be accessed in another package.
   but it can be if we extend the class in which it is declared in another package too.
   And it cannot be invoked using object reference 
   it is to be invoked in methods.


   Types of objects

   -Bean objects(Java Bean/Enterprise Java Bean)
   -POJO(Plain Old Java Objects)
   -Value Objects/Transfer Objects//contains only getters and setters

   Business Objects
   -EJB Objects
   -DAO (Data Access Objects)
   Database Objects// implementation of methods to use trnsfer objects




EX-

package p5;

public class EmpVivo {
	private String name;
	private long salary,tax;
	
	public long getTax() {
		return tax;
	}
	public void setTax(long tax) {
		this.tax = tax;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getSalary() {
		return salary;
	}
	public void setSalary(long salary) {
		this.salary = salary;
	}
}


public class EmpBO {
	public void calTax(EmpVivo e1) {
		if(e1.getSalary()<300000) {
			e1.setTax(0);
		}
		else if(e1.getSalary()<=500000) {
			long l;
			l=(long)((e1.getSalary()-300000)*0.05);
			e1.setTax(l);
		}
		else if(e1.getSalary()<=1000000) {
			long l;
			l=(long)((e1.getSalary()-500000)*0.2)+10000;
			e1.setTax(l);
		}
		else {
			long l;
			l=(long)((e1.getSalary()-1000000)*0.3)+110000;
			e1.setTax(l);
		}
	}

}


public class EmpVivoDemo {
	public static void main(String[] args) {
		EmpVivo ob=new EmpVivo();
		EmpBO ob1=new EmpBO();
		System.out.println("Enter name and salary");
		ob.setName(p1.Read.obj.nextLine());
		ob.setSalary(p1.Read.obj.nextLong());
		ob1.calTax(ob);
		System.out.println("Tax"+ob.getTax());
	}
	}









   Inheritance

   Create 4 classes
   Addition 
   subtraction
   Multiplication
   Division
   use num1,num2,num3,constructors,setData(),calculate() AND display() 
   in each class


   package p5;

public class Addition extends Arithmetic {
	
		public void calculate() {
			num3=num1+num2;
		}
		
}
		

public class Sub extends Arithmetic {
	
	public void calculate() {
		num3=num1-num2;
	}
	
}

public class Mult extends Arithmetic {
	
	public void calculate() {
		num3=num1*num2;
	}
	

}

public class Div extends Arithmetic {
	
	public void calculate() {
		num3=num1/num2;
	}
	

}

public class Arithmetic {
	protected int num1,num2,num3;
	public void setData(int num1,int num2) {
		this.num1=num1;
		this.num2=num2;
	}
	public void calculate() {
		
	}
	public void display() {
		System.out.println(num3);
	}

}

public class DemoInheritance {
	
	public static void main(String[] args) {

	Arithmetic ob=new Addition();
	ob.setData(10,20);
	ob.calculate();
	ob.display();
	
	ob=new Sub();
	ob.setData(12,10);
	ob.calculate();
	ob.display();
}
}

