package p1;
public class Numbers{
	public String toRomanValue(long num){
		return"";

	}

	public static String toWords(long num){
		String[]units={"","one","two","three","four","five","six","seven","eight","nine","ten"};
		String[]tens={"","ten","twenty","thirty","forty","fifty","sixty","seventy","eigthy","ninty"}
		String[]s_value={"crore","lakh","thousand","hundred","only"}
		long[] n_value={10000000,100000,1000,100,1};
		String word="";

		for(int i=0;i<n_value.length;i++){
			int n1=(int)(num/n_value[i]);
			num%=n_value[i];
			if(n1>0){
				if(n1>19){
					word=word+tens[n1/10]+units[n1%10]+s_value[i];

				}
				else{
					word=word+units[n1]+s_value[i];
				}
			}
		}
		return word.trim();
	}

}





public class Day_2_Demo1 {
	public static void main(String []args){
		System.out.println(Numbers.toWords(10));
	}
}


/*create a class for the following 
addition which contains 3 instance variables num1, num2 ,num3 and following methods
set data method, read data method ,cal method and display method
employee class which contains employee name, age, city as instance variables and method to set data, method to display data 
(create a array of employees and sort the employees name wise ,city wise and age wise)

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/* Scope of the variables
scope and access specifier both are two different concepts
value exist or not is termed as scope 
whether it can be accessed or not is termed as access specifier


Different scopes available in java

1. Block Scope: a. any variable declare inside a block like if block, try block;
b. Block variable declared in one block can be redeclared within another block;
c. Method variablename and block variablename cannot be the same in the same methods;
d. Block Variables are not auto initialised;
e. In java a variable used without initialisation is a compiler error;

2. Method Scope: a. any variable declared within a method outside internal block;
b. The variables declared in one method cannot be used in another method but the same variable can be declared in different methods.
c. Method variables are also not auto initialised.
d. Method variable name and instance variable name can be same.In this case higher prirority is given to the method variable;
e. All the formal variables are also called as method variables.
f. In case the formal variable and instance variable name are same then use 'this' keyword with instance variable.
g. The memory is released as soon as a block/method completes the job.
h. Both block and method variables are stored in stack.

3. a. Instance Scope:variables declared inside the class and outside the method ;
b. It is recognised only in the respective instance or every instance has the separate copy of the instance variable; 
c. Instance variables are recognised by the "this" keyword;
d. all instance variable are auto initialised;
e. they are stored in heap;

4. a. Class Scope:variables declared inside the class and outside the method but with static keyword;
b. it can have only one copy for all the instances;
c. class variables exists as soon as the application starts,even before the object creation;
d. class variable can be called using class or object reference;
e. they are stored in class segment;

types of methods

1. Managerial Methods:
a.automatically called at the time of garbage collection.(Constructors and destructors)
2. Instance Methods:
a. A method without the keyword static.
b. it is called only after the object is created.
c. instance method can access both instance and class variables.
d. Instance method can call another instance method or class method.

3. Class Methods:
a. A method with the keyword static;
b. it can be called without or after object creation;
c. static method can access only class variables however, instance variable can be accessed through the object reference.
d. class methods can call another class method but not instance method however,it can be called through the object reference;

Note:A method signature is made up of 
name of the method.
no. of arguments
type of arguments
order of arguments
No two methods can have the same signatures.



Type of Arguments

1. Primitive
2. Object Type
3. Wrapper Class
4. Arrays 
5.Infinite line argument(every method can have only 1 infinite line argument, only at the end of all the arguments and are accessed in the form arrays).

create a matrix class to add two matrix
